#!/usr/bin/env python3
#<todo>
# Passer full sympy pour les marquages de places car sympify ne conserve pas les propriétés des symboles 
# Par exemple la propriété de positivité de r1 donnée à la création du symbole n'est pas conservée par a=sympify('r1')

import numpy as np
from sympy import *
from copy import deepcopy
import random

class CPN():

    def __init__(self,name=''):
        self.cpnName=name
        self.listPlaces={}
        self.list_places=[]
        self.listTrans=[]
        self.listArcs=[]
        self.listColors=[]

    def addPlace(self,place):
        self.listPlaces[place.placeName]=place

    def addTransition(self,trans):
        self.listTrans.append(trans)

    def addArc(self,arc):
        self.listArcs.append(arc)

    def addColor(self,color):
        self.listColors.append(color)

    def __str__(self):
        return("""CPN: {}\nPlaces: {}\nTransitions: {}\nColors: {}""".format(self.cpnName,self.listPlaces.keys(),self.listTrans,self.listColors))

    def generateXML(self,Id):
        """generate a xml file for cpn tool"""
        #Init of the string containing xml code
        xml="""<?xml version="1.0" encoding="iso-8859-1"?>\n"""
        xml+="""<!DOCTYPE workspaceElements PUBLIC "-//CPN//DTD CPNXML 1.0//EN" "http://cpntoold.org/DTD/6/cpn.dtd">\n"""
        xml+="""<workspaceElements>\n"""
        xml+="""<generator tool="CPN Tools" version="4.0.1" format="6"/>\n"""
        xml+="""<cpnet>\n"""

        ###Declarations
        xml+="""<globbox>\n"""

        #Priorities
        xml+="""<block>\n"""
        xml+="""<id>Standard priorities</id>\n"""
        xml+="""<ml>val P_HIGH = 100;\n<layout>val P_HIGH = 100;</layout>\n</ml>\n"""
        xml+="""<ml>val P_NORMAL = 1000;\n<layout>val P_NORMAL = 1000;</layout>\n</ml>\n"""
        xml+="""<ml>val P_LOW = 10000;\n<layout>val P_LOW = 10000;</layout>\n</ml>\n"""
        xml+="""</block>\n"""

        #Colors
        xml+="""<block>\n"""
        xml+="""<id>Declarations</id>\n"""
        vars=""
        for color in self.listColors:
            xml+="""<color>\n"""
            xml+="""<id>{}</id>\n""".format(color.colName)
            xml+="""<enum>\n"""
            enum=""
            for item in color.colset:
                xml+="""<id>{}</id>\n""".format(item)
                enum+=""" {} |""".format(item)
            xml+="""</enum>\n"""
            xml+="""<layout>colset {} = with{};</layout>""".format(color.colName,enum[:-2])
            xml+="""</color>\n"""

            #Var associated with colors
            vars+="""<var>\n<type>\n<id>{}</id>\n</type>\n""".format(color.colName)
            vars+="""<id>{}</id>\n""".format(color.colVar)
            vars+="""<layout>var {} : {};</layout>\n""".format(color.colVar,color.colName)
            vars+="""</var>\n"""

        xml+=vars
        xml+="""</block>\n"""
        xml+="""</globbox>\n"""
        ###

        ###Main Page
        idPage=Id
        Id+=1
        xml+="""<page id="ID{}">\n<pageattr name="Net"/>\n""".format(idPage)

        #Places
        places=""
        for pl in self.listPlaces:
            currPl=self.listPlaces[pl]
            places+="""<place id="ID{}">\n""".format(currPl.id)
            places+="""<text>{}</text>\n""".format(currPl.placeName)
            places+="""<ellipse w="60.0" h="40.0"/>\n""" #Just for visual conveniance
            places+="""<type>\n"""
            places+="""<posattr x="50.0" y="-23.0"/>\n"""
            places+="""<text tool="CPN Tools" version="4.0.1">{}</text>\n""".format(currPl.placeColor.colName)
            places+="""</type>\n"""
            places+="""<initmark>\n"""
            places+="""<posattr x="40.0" y="23.0"/>\n"""
            if currPl.initMarking=='':
                places+="""<text tool="CPN Tools" version="4.0.1"/>\n"""
            else:
                places+="""<text tool="CPN Tools" version="4.0.1">{}</text>\n""".format(currPl.initMarking)
            places+="""</initmark>\n</place>\n"""

        xml+=places

        #Transitions

        transi=""
        for tr in self.listTrans:
            transi+="""<trans id="ID{}" explicit="false">\n""".format(tr.id)
            transi+="""<text>{}</text>\n""".format(tr.transName)
            transi+="""<box w="60.0" h="40.0"/>\n""" #For visual conveniance
            transi+="""</trans>\n"""

        xml+=transi

        #Arcs

        arcs=""
        for a in self.listArcs:
            arcs+="""<arc orientation="{}" order="1">\n""".format(a.type)
            arcs+="""<transend idref="ID{}"/>\n""".format(a.trans.id)
            arcs+="""<placeend idref="ID{}"/>\n""".format(a.place.id)
            arcs+="""<annot>\n"""
            arcs+="""<text tool="CPN Tools" version="4.0.1">{}</text>\n""".format(a.expr)
            arcs+="""</annot>\n</arc>\n"""

        xml+=arcs


        xml+="""</page>\n"""

        #instance
        idInstance=Id
        Id+=1
        xml+="""<instances>\n<instance id="ID{}" page="ID{}"/>\n</instances>\n""".format(idInstance,idPage)

        #binder
        idBinder=Id
        Id+=1
        xml+="""<binders>\n"""
        xml+="""<cpnbinder id="ID{}" x="257" y="122" width="600" height="400">\n""".format(idBinder)
        idSheet=Id
        Id+=1
        xml+="""<sheets>\n"""
        xml+="""<cpnsheet id="ID{}" panx="-6.0" pany="-5.0" zoom="1.0" instance="ID{}">\n""".format(idSheet,idInstance)
        xml+="""<zorder>\n<position value="0"/>\n</zorder>\n"""
        xml+="""</cpnsheet>\n</sheets>\n"""
        xml+="""<zorder>\n<position value="0"/>\n</zorder>\n"""
        xml+="""</cpnbinder>\n</binders>\n"""

        #Monitors
        xml+="""<monitorblock name="Monitors"/>\n"""


        #End of xml code
        xml+="""</cpnet>\n</workspaceElements>\n"""

        #create output file
        output=open("{}.cpn".format(self.cpnName),'w+')
        output.write(xml)


    def genVars(self):
        symVars=[]
        for color in self.listColors:
            if color==ROBOTS or color==TASKS:
                for c in color.colset:
                    symVars.append("{}=symbols('{}',positive=True)".format(c,c,c))
            symVars.append("{}=symbols('{}')".format(color.colVar,color.colVar,color.colVar))
        return(symVars)

    def isEnabled(self,trans):
        for a in trans.listArcs:
            if a.type=="PtoT":
                if a.expr not in self.listPlaces[a.place.placeName].marking:
                    return(False)
        return(True)

    def fire(self,trans):
        for a in trans.listArcs:
            if a.type=="PtoT":
                newmark=sympify(self.listPlaces[a.place.placeName].marking+'-'+a.expr)
                self.listPlaces[a.place.placeName].marking=str(newmark)
            else:
                newmark=sympify(self.listPlaces[a.place.placeName].marking+'+'+a.expr)
                self.listPlaces[a.place.placeName].marking=str(newmark)


    def marking_r(self):
        """return the current marking of the robot places"""
        nbTaskPlaces=0
        for z in Y:
            nbTaskPlaces+=len(doTask.get(z))
        m=[]
        for pl in self.list_places[nbTaskPlaces:]:
            p=self.listPlaces[pl]
            if p.marking!='':
                m.append(p.marking)
            else:
                m.append('0')
        return(m)


    def match(self,marking):
        """with marking the marking vector of the robot places"""
        #séparer les marquages dans des listes différentes ou trouver un moyen de checker si le marquage est au moins supérieur à un autre marquage
        path=[]
        while self.marking_r()!=marking:
            enable=[]
            for t in self.listTrans:
                if self.isEnabled(t):
                    enable.append(t)
            notok=[]
            for e in enable:
                for i in range(len(marking)):
                    if self.marking_r()[i]==marking[i]:
                        for arc in e.listArcs:
                            if arc.place.placeName=='p'+str(i+1) and arc.type=="PtoT":
                                if sympify(marking[i]+'-'+arc.expr)==0:
                                    notok.append(e)
                if "exec" in e.transName:
                    notok.append(e)
            for n in notok:
                enable.pop(enable.index(n))
            if enable!=[]:
                eff=random.choice(enable)
            self.fire(eff)
            path.append(eff)
        return(path)

class Place():

    def __init__(self,name,col,mark,id):
        self.placeName=name
        self.placeColor=col
        self.initMarking=mark
        self.marking=None
        self.id=id

    def __repr__(self):
        return("{}".format(self.placeName))

    def __str__(self):
        return("Place: {}, {}, initMarking {}".format(self.placeName,self.placeColor,self.initMarking))

    def __eq__(self,other):
        return(self.id==other.id)


class Transition():

    def __init__(self,name,id):
        self.transName=name
        self.id=id
        self.listArcs=[]

    def __repr__(self):
        return(self.transName)

    def __eq__(self,other):
        return(self.id==other.id)

    def addArc(self,arc):
        self.listArcs.append(arc)



class Arc():

    def __init__(self,typ,P,T,exp=''):
    #typ must be PtoT or TtoP
        try:
            if typ=="PtoT" or typ=="TtoP":
                self.type=typ
            else:
                raise ValueError("Wrong Arc type: {}".format(typ))
        except ValueError as ve:
            print(ve)

        self.expr=exp
        self.place=P
        self.trans=T

    def __repr__(self):
        if self.type=='PtoT':
            return("{} from {} to {}, expr={}".format(self.type,self.place.placeName,self.trans.transName,self.expr))
        else:
            return("{} from {} to {}, expr={}".format(self.type,self.trans.transName,self.place.placeName,self.expr))


class Color():

    def __init__(self,name,items,var):
        self.colName=name
        self.colset=items
        self.colVar=var

    def __repr__(self):
        return("Colset {}".format(self.colName))

    def __eq__(self,other):
        return self.colName==other.colName



################
#    Data      #
################
P=['p1','p2','p3','p4','p5','p6'] #Set of places
initmark=['r1','','','','r2','']
R=['r1','r2'] #Set of robots
Y=['y1','y2'] #Set of tasks

#Define which tasks each robot can do
capability={'r1':{'y1':True,'y2':False},'r2':{'y1':True,'y2':True}}

# Dictionnary defining the possibility of travel for each robot
travel={
'r1':{
'p1':{'p1':False,'p2':True,'p3':False,'p4':False,'p5':False,'p6':False},
'p2':{'p1':True,'p2':False,'p3':True,'p4':False,'p5':False,'p6':False},
'p3':{'p1':False,'p2':True,'p3':False,'p4':True,'p5':False,'p6':False},
'p4':{'p1':False,'p2':False,'p3':True,'p4':False,'p5':False,'p6':False},
'p5':{'p1':False,'p2':False,'p3':False,'p4':False,'p5':False,'p6':False},
'p6':{'p1':False,'p2':False,'p3':False,'p4':False,'p5':False,'p6':False}
},
'r2':{
'p1':{'p1':False,'p2':True,'p3':False,'p4':False,'p5':False,'p6':True},
'p2':{'p1':True,'p2':False,'p3':False,'p4':False,'p5':False,'p6':False},
'p3':{'p1':False,'p2':False,'p3':False,'p4':False,'p5':False,'p6':True},
'p4':{'p1':False,'p2':False,'p3':False,'p4':False,'p5':True,'p6':False},
'p5':{'p1':False,'p2':False,'p3':False,'p4':True,'p5':False,'p6':True},
'p6':{'p1':True,'p2':False,'p3':True,'p4':False,'p5':True,'p6':False}
}
}

doTask={'y1':['p2','p3'],'y2':['p4']}


################
# Useful fonctions to exploit data
################

def canTravel(robot,orig,dest):
    """Return True if robot can travel from orig to dest, False if not"""
    return(travel.get(robot).get(orig).get(dest))

def whereTask(task):
    """Return a list of places where task is to be executed"""
    return(doTask.get(task))

def canDo(robot,task):
    """Return True if robot can execute task, False if not"""
    return(capability.get(robot).get(task))

def greater(mark1,mark2):
    """Return True if marking mark1 > marking mark2"""
    great=False
    for i in range(len(mark1)):
        pass


################


test=CPN('test')

Id=1

# Add colsets
ROBOTS=Color('ROBOTS',R,'x')
test.addColor(ROBOTS)
TASKS=Color('TASKS',Y,'z')
test.addColor(TASKS)


#Add places for the robots
for i in range(len(P)):
    newplace=Place(P[i],ROBOTS,initmark[i],Id)
    newplace.marking=newplace.initMarking.replace('`','*')
    Id+=1
    test.addPlace(newplace)

#Add places for the tasks
for y in Y:
    pl=whereTask(y)
    for i in range(len(pl)):
        newplace=Place('Task_'+y+'_'+str(i+1),TASKS,'1`'+y,Id)
        newplace.marking=newplace.initMarking.replace('`','*')
        Id+=1
        test.addPlace(newplace)
        #Create associated transition for the execution of the task
        newtrans=Transition('exec'+y+'_'+str(i+1),Id)
        Id+=1
        #Creation of the 3 arcs bounded with this transition
        newarcTasktoT=Arc('PtoT',newplace,newtrans,y)
        test.addArc(newarcTasktoT)
        newtrans.addArc(newarcTasktoT)
        for r in R:
            if canDo(r,y):
                expr=r
                newarcPtoT=Arc('PtoT',test.listPlaces[pl[i]],newtrans,expr)
                newarcTtoP=Arc('TtoP',test.listPlaces[pl[i]],newtrans,expr)
                newtrans.addArc(newarcPtoT)
                test.addArc(newarcPtoT)
                newtrans.addArc(newarcTtoP)
                test.addArc(newarcTtoP)
        test.addTransition(newtrans)

#Add transitions for the robots
for pOrig in P:
    for pDest in P:
        valid=[]
        for r in R:
            if canTravel(r,pOrig,pDest):
                expr=r
                #Creation of the transition
                newtrans=Transition(pOrig+'to'+pDest+'_'+r,Id)
                Id+=1
                #creation of the two arcs bounded with the transition
                newarcPtoT=Arc('PtoT',test.listPlaces[pOrig],newtrans,expr)
                newarcTtoP=Arc('TtoP',test.listPlaces[pDest],newtrans,expr)
                test.addArc(newarcPtoT)
                newtrans.addArc(newarcPtoT)
                test.addArc(newarcTtoP)
                newtrans.addArc(newarcTtoP)
                test.addTransition(newtrans)

########################
# Creation of matrixes #
########################


Cm={};Prem={}
for pl in test.listPlaces:
    ligne={}
    prev={}
    for a in test.listArcs:
        if a.place.placeName==pl:
            t=a.trans.transName
            if a.type=="PtoT":
                if t in ligne.keys():
                    ligne[t]=ligne[t]+'-'+a.expr
                else:
                    ligne[t]='-'+a.expr

                if t in prev.keys():
                    prev[t]=prev[t]+'+'+a.expr
                else:
                    prev[t]=a.expr
            else:
                if t in ligne.keys():
                    ligne[t]=ligne[t]+'+'+a.expr
                else:
                    ligne[t]=a.expr
    Cm[pl]=ligne
    Prem[pl]=prev

#print(C)
#print(Pre)

##############################
# Creation of marking vector #
##############################

M0={}
for pl in test.listPlaces:
    p=test.listPlaces[pl]
    M0[p.placeName]=p.initMarking.replace('`','*')

################
#     XML      #
################

test.generateXML(Id)


#####################
#  Sympy Variables  #
#####################
# Has to be executed in __main__ in order to exploit the variables

for var in test.genVars():
    exec(var)


test.list_places=list(test.listPlaces.keys())
test.list_places.sort()

list_trans=[]
for t in test.listTrans:
    list_trans.append(t.transName)

# Creation of the incidence matrix and initial marking vector for sympy
M=[]
Mr=[]
initM=[]
for p in test.list_places:
    ligne=[]
    for t in list_trans:
        if t in Cm[p].keys():
            ligne.append(Cm[p][t])
        else:
            ligne.append(0)
    M.append(ligne)
    Mr.append(ligne[nbTaskPlaces:])
    if M0[p]!='':
        initM.append(M0[p])
    else:
        initM.append(0)

C=Matrix(M)
Cr=Matrix(Mr[nbTaskPlaces:])
m0=Matrix(initM)
m0r=Matrix(initM[nbTaskPlaces:])

sig=[]
for i in range(len(list_trans)-nbTaskPlaces):
    symb='s'+str(i)
    exec("{}=symbols('{}')".format(symb,symb))
    exec("sig.append({})".format(symb))

###
#calcul
#TargetMark=Matrix([0,r1,r2,0,0,0])

#Vect=linsolve((Cr,TargetMark-m0r),sig)
#print(Vect)


################
#    Tests     #
################


#print(test)
#print(list(test.listPlaces.keys()))
#print(len(test.listPlaces))
#print(test.listArcs)
#print(len(test.listTrans))
#print(test.listTrans)
#print(test.listArcs)
#print(xml)
#print(M)
#print(list_places)
#print(list_trans)
#print(C)
#print(m0)
#print(test.listColors)
